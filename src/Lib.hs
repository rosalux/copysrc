module Lib
    ( someFunc
    ) where
import System.IO
import System.Environment
import System.Clipboard
import Control.Monad

someFunc :: IO ()
someFunc = putStrLn "someFunc"
