module Main where

import Lib
import System.IO
import Control.Monad
import System.Environment
import System.Clipboard

main :: IO ()
main = do
  args <- getArgs
  let file = args !! 0
  handle <- openFile file ReadMode
  cont <- hGetContents handle
  setClipboardString cont
  let answer = (file ++ " has been copied to the clipboard")
  putStrLn answer
  hClose handle
